﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataAccess
{
    public class MapperConcepto
    {
        private Acceso acceso = new Acceso();

        public List<Concepto> Consultar()
        {
            DataTable tabla = new DataTable();
            
            tabla = acceso.Leer("ConsultarConceptos");
            List<Concepto> conceptos = new List<Concepto>();

            foreach (DataRow item in tabla.Rows)
            {
                Concepto con = new Concepto();

                con.Id = int.Parse(item["Id"].ToString());
                con.Descripcion = item["Descripcion"].ToString();
                con.PorcentajeAplicado = (int)item["PorcentageAplicado"];

                conceptos.Add(con);
            }

            return conceptos;
        }

        public Concepto Consultar(int id)
        {
            DataTable tabla = new DataTable();

            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@id", id));
            tabla = acceso.Leer("GetConcepto", parametros);
            Concepto con = new Concepto();
            DataRow fila = tabla.Rows[0];

            con.Id = (int)fila["id"];
            con.Descripcion = fila["Descripcion"].ToString();
            con.PorcentajeAplicado = (int)fila["PorcentageAplicado"];

            return con;
        }

        public int Crear(Concepto con)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@desc", con.Descripcion));
            parametros.Add(acceso.CrearParametros("@porc", con.PorcentajeAplicado));

            return acceso.Escribir("InsertarConcepto", parametros);
        }

        public int Modificar(Concepto con)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@id", con.Id));
            parametros.Add(acceso.CrearParametros("@desc", con.Descripcion));
            parametros.Add(acceso.CrearParametros("@porc", con.PorcentajeAplicado));

            return acceso.Escribir("ModificarConcepto", parametros);
        }

        public int Eliminar(int id)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@id", id));

            return acceso.Escribir("EliminarConcepto", parametros);
        }
    }
}
