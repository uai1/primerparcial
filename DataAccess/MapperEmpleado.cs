﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataAccess
{
    public class MapperEmpleado
    {
        private Acceso acceso = new Acceso();
        public List<Empleado> Consultar()
        {
            DataTable tabla = acceso.Leer("ConsultarEmpleados");
            List<Empleado> empleados = new List<Empleado>();

            foreach (DataRow item in tabla.Rows)
            {
                Empleado empleado = new Empleado();

                empleado.Id = (Int32)item["Id"];
                empleado.Legajo = (Int32)item["Legajo"];
                empleado.Nombre = item["Nombre"].ToString();
                empleado.Apellido = item["Apellido"].ToString();
                empleado.Cuil = (Int32)item["Cuil"];

                empleados.Add(empleado);
            }

            return empleados;
        }

        public Empleado Consultar(int Id)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@Id", Id));
            DataTable tabla = acceso.Leer("GetEmpleado", parametros);

            DataRow item = tabla.Rows[0];
            Empleado empleado = new Empleado();

            empleado.Id = (Int32)item["Id"];
            empleado.Legajo = (Int32)item["Legajo"];
            empleado.Nombre = item["Nombre"].ToString();
            empleado.Apellido = item["Apellido"].ToString();
            empleado.Cuil = (Int32)item["Cuil"];

            return empleado;
        }

        public int Crear(Empleado emp)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@Nom", emp.Nombre));
            parametros.Add(acceso.CrearParametros("@Ape", emp.Apellido));
            parametros.Add(acceso.CrearParametros("@Lega", emp.Legajo));
            parametros.Add(acceso.CrearParametros("@Cuil", emp.Cuil));

            return acceso.Escribir("InsertarEmpleado", parametros);
        }

        public int Eliminar(int Id)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@Id", Id));

            return acceso.Escribir("EliminarEmpleado", parametros);
        }

        public int Modificar(Empleado emp)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@Id", emp.Id));
            parametros.Add(acceso.CrearParametros("@Nom", emp.Nombre));
            parametros.Add(acceso.CrearParametros("@Ape", emp.Apellido));
            parametros.Add(acceso.CrearParametros("@Lega", emp.Legajo));
            parametros.Add(acceso.CrearParametros("@Cuil", emp.Cuil));

            return acceso.Escribir("ModificarEmpleado", parametros);
        }
    }
}
