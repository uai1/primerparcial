﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq.Expressions;

namespace DataAccess
{
    internal class Acceso
    {
        private SqlConnection conection;

        public void AbrirConeccion()
        {
            conection = new SqlConnection(@"Integrated Security=SSPI;Initial Catalog=Universidad;Data Source=AR-IT18120\MSSQLSERVER01;");

            conection.Open();
        }

        public void CerrarConeccion()
        {
            conection.Close();
            conection.Dispose();
        }

        public SqlCommand CrearComando(string nombre, List<SqlParameter> parametros = null)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conection;
            comando.CommandText = nombre;
            comando.CommandType = CommandType.StoredProcedure;

            if (parametros != null && parametros.Count != 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;
        }

        public DataTable Leer(string nombre, List<SqlParameter> parametros = null)
        {
            AbrirConeccion();

            DataTable tabla = new DataTable();
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(nombre, parametros);

            adaptador.Fill(tabla);
            CerrarConeccion();
            return tabla;
        }

        public int Escribir(string nombre, List<SqlParameter> parametros)
        {
            AbrirConeccion();

            int filasAf = 0;
            SqlCommand comando = CrearComando(nombre, parametros);

            try
            {
                filasAf = comando.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                filasAf = -1;
            }

            CerrarConeccion();
            return filasAf;
        }

        public SqlParameter CrearParametros(string nombre, object valor)
        {
            var tipo = valor.GetType();
            SqlParameter parametros = new SqlParameter();
            parametros.Value = valor;
            parametros.ParameterName = nombre;
            switch (tipo.Name)
            {
                case "Int32":
                    parametros.SqlDbType = SqlDbType.Int;
                    break;

                case "String":
                    parametros.SqlDbType = SqlDbType.VarChar;
                    break;
            }
            return parametros;
        }
    }
}
