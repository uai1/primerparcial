﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataAccess
{
    public class MapperRecibo
    {
        private Acceso acceso = new Acceso();

        public int Crear(Recibo rec)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@Emp_Id", rec.Empleado_Id));
            parametros.Add(acceso.CrearParametros("@SueBru", rec.SueldoBruto));
            parametros.Add(acceso.CrearParametros("@SueNet", rec.SueldoNeto));
            parametros.Add(acceso.CrearParametros("@TotDes", rec.TotalDescuentos));
            parametros.Add(acceso.CrearParametros("@Fecha", rec.Fecha));

            return acceso.Escribir("InsertarRecibo", parametros);
        }

        public List<Recibo> Consultar(int empleado_Id)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametros("@Emp_Id", empleado_Id));

            DataTable tabla = acceso.Leer("ConsultarRecibos", parametros);
            List<Recibo> recibos = new List<Recibo>();

            foreach (DataRow item in tabla.Rows)
            {
                Recibo rec = new Recibo();
                rec.Id = int.Parse(item["Id"].ToString());
                rec.Empleado_Id = int.Parse(item["Id_Empleado"].ToString());
                rec.SueldoBruto = int.Parse(item["SueldoBruto"].ToString());
                rec.SueldoNeto = int.Parse(item["SueldoNeto"].ToString());
                rec.TotalDescuentos = int.Parse(item["TotalDescuentos"].ToString());

                recibos.Add(rec);
            }

            return recibos;
        }
    }
}
