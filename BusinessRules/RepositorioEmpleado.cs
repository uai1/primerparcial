﻿using DataAccess;
using Entities;
using System;
using System.Collections.Generic;

namespace BusinessRules
{
    public class RepositorioEmpleado
    {
        MapperEmpleado mapper = new MapperEmpleado();

        public List<Empleado> ListarEmpleados()
        {
            return mapper.Consultar();
        }

        public Empleado GetEmpleado(int id)
        {
            return mapper.Consultar(id);
        }

        public int InsertarEmpleado(Empleado emp)
        {
            return mapper.Crear(emp);
        }

        public int ModificaEmpleado(Empleado emp)
        {
            return mapper.Modificar(emp);
        }

        public int EliminaEmpleado(int id)
        {
            return mapper.Eliminar(id);
        }
    }
}
