﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using DataAccess;
using Entities;

namespace BusinessRules
{
    public class RepositorioRecibo
    {
        MapperRecibo mapper = new MapperRecibo();

        public List<Recibo> ConsultaRecibo(int Empleado_Id)
        {
            return mapper.Consultar(Empleado_Id);
        }

        public int InsertarRecibo(Recibo rec)
        {
            Recibo rec_final = CalcularSueldoNeto(rec);

            return mapper.Crear(rec_final);
        }

        private List<float> CalcularTotalDescuentos(Recibo rec)
        {
            List<float> descuentos = new List<float>();

            foreach (var item in rec.Conceptos)
            {
                float descuento = (rec.SueldoBruto * item.PorcentajeAplicado) / 100;

                descuentos.Add(descuento);
            }

            return descuentos;
        }

        private Recibo CalcularSueldoNeto(Recibo rec)
        {
            List<float> descuentos = CalcularTotalDescuentos(rec);
            float sueldoNeto = rec.SueldoBruto;
            rec.TotalDescuentos = 0;

            foreach (float item in descuentos)
            {
                sueldoNeto += item;

                rec.TotalDescuentos++;
            }

            rec.SueldoNeto = sueldoNeto;
            return rec;
        }
    }
}
