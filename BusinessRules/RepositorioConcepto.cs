﻿using Entities;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessRules
{
    public class RepositorioConcepto
    {
        MapperConcepto mapper = new MapperConcepto();

        public List<Concepto> ListarConceptos()
        {
            return mapper.Consultar();
        }

        public Concepto GetConcepto(int id)
        {
            return mapper.Consultar(id);
        }

        public int InsertarConcepto(Concepto con)
        {
            return mapper.Crear(con);
        }

        public int ModificaConcepto(Concepto con)
        {
            return mapper.Modificar(con);
        }

        public int EliminaConcepto(int id)
        {
            return mapper.Eliminar(id);
        }
    }
}
