﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Concepto
    {
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public int PorcentajeAplicado { get; set; }
    }
}
