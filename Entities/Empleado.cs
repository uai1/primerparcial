﻿using System;

namespace Entities
{
    public class Empleado
    {
        public int Id { get; set; }

        public int Legajo { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public int Cuil { get; set; }
    }
}
