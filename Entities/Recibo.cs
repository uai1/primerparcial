﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Recibo
    {
        public int Id { get; set; }

        public DateTime Fecha { get; set; }

        public int Empleado_Id { get; set; }

        public float SueldoBruto { get; set; }

        public float SueldoNeto { get; set; }

        public float TotalDescuentos { get; set; }

        public List<Concepto> Conceptos { get; set; }
    }
}
