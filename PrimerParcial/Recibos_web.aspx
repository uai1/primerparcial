﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recibos_web.aspx.cs" Inherits="PrimerParcial.Recibos_web" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div style="border: solid 1px black">
                <div class="field">
                    <label id="lblSueldoBruto">Sueldo Bruto: </label>
                    <asp:TextBox ID="txtSueldoBru" runat="server"/>

                    <label id="lblFecha" style="margin-left: 30px;">Fecha: </label>
                    <asp:TextBox TextMode="Month" runat="server" ID="txtDate" />
                </div>
                <br />

                <span>Deseleccione los que no correspondan: </span>
                <asp:gridview id="grdConceptos" runat="server" autogeneratecolumns="false" 
                headerstyle-backcolor="#3ac0f2" headerstyle-forecolor="white" width="462px">
                    <columns>
                       <asp:templatefield>
                            <itemtemplate>            
                                <input class="chkview" runat="server" type="checkbox" checked="checked"/>
                            </itemtemplate>
                        </asp:templatefield>
                        <asp:boundfield datafield="id" headerstyle-width="15" >
                        </asp:boundfield>
                        <asp:boundfield datafield="descripcion" headertext="descripcion" headerstyle-width="200" >
                        </asp:boundfield>
                        <asp:boundfield datafield="porcentajeaplicado" headertext="porcentaje" headerstyle-width="70" >
                        </asp:boundfield>
                    </columns>
                    <headerstyle backcolor="#3ac0f2" forecolor="white"></headerstyle>
                </asp:gridview>

                <asp:button id="btngenerar_recibo" text="Generar Recibo" runat="server" OnClick="btnGenerar_Recibo_Click" />
             </div>

            <div>
                <h2>Lista de recibos para empleado</h2>
                <br />

                <asp:GridView id="grdRecibos" runat="server" AutoGenerateColumns="False" 
                HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" Width="462px">
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" HeaderStyle-Width="20" >
                        </asp:BoundField>
                        <asp:BoundField DataField="Empleado_Id" HeaderText="Id_Empleado" HeaderStyle-Width="20" >
                        </asp:BoundField>
                        <asp:BoundField DataField="SueldoBruto" HeaderText="Sueldo Bruto" HeaderStyle-Width="70" >
                        </asp:BoundField>
                        <asp:BoundField DataField="SueldoNeto" HeaderText="Sueldo Neto" HeaderStyle-Width="70" >
                        </asp:BoundField>
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" HeaderStyle-Width="70" >
                        </asp:BoundField>
                        <asp:BoundField DataField="TotalDescuentos" HeaderText="Total Descuentos" HeaderStyle-Width="70" >
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
                </asp:GridView>
            </div>
        </div>
                             <asp:button id="btnAtras" runat="server" Text="Atras" style="width: 100px; height:30px;" OnClick="btnAtras_Click" />
                    <br />
    </form>
</body>
</html>
