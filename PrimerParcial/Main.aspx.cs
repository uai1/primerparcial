﻿using BusinessRules;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrimerParcial
{
    public partial class Main : System.Web.UI.Page
    {
        RepositorioEmpleado repo_Emp = new RepositorioEmpleado();
        RepositorioConcepto repo_Con = new RepositorioConcepto();

        #region Empleado
        protected void Page_Load(object sender, EventArgs e)
        {
            btnCancelar_Empleado.Visible = false;
            btnGuardar_Empleado.Visible = false;
            Bound();
        }

        public void Bound()
        {
            grdEmpleados.DataSource = repo_Emp.ListarEmpleados();
            grdEmpleados.DataBind();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            if (Verificacion())
            {
                Empleado emp = new Empleado();
                emp.Legajo = Convert.ToInt32(txtLeg.Text);
                emp.Nombre = txtNom.Text;
                emp.Apellido = txtApe.Text;
                emp.Cuil = Convert.ToInt32(txtCuil.Text);

                repo_Emp.InsertarEmpleado(emp);

                txtNom.Text = string.Empty;
                txtApe.Text = string.Empty;
                txtCuil.Text = string.Empty;
                txtLeg.Text = string.Empty;

                Bound();
            }
        }

        private bool Verificacion()
        {
            if (txtApe.Text != string.Empty && 
                txtNom.Text != string.Empty &&
                txtLeg.Text != string.Empty &&
                txtCuil.Text != string.Empty)
            {
                return true;
            }

            ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), "alert('Por Favor verifique que todos los campos tengan informacion')", true);
            return false;
        }

        protected void grdEmpleados_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int Id = int.Parse(grdEmpleados.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text);
            Empleado emp = repo_Emp.GetEmpleado(Id);
            switch (e.CommandName)
            {
                case "Select":

                    btnCancelar_Empleado.Visible = true;
                    btnGuardar_Empleado.Visible = true;
                    btnAgregar_Empleado.Visible = false;

                    hidId.Text = emp.Id.ToString();
                    txtNom.Text = emp.Nombre;
                    txtApe.Text = emp.Apellido;
                    txtCuil.Text = emp.Cuil.ToString();
                    txtLeg.Text = emp.Legajo.ToString();
                    break;
                case "Delete":

                    repo_Emp.EliminaEmpleado(Id);
                    break;

                case "Recibos":
                    Response.Redirect("Recibos_web.aspx?param=" + Id);
                    break;
            }
            Bound();
        }

        protected void grdEmpleados_RowDeleting(object sender, GridViewDeleteEventArgs e){}

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (Verificacion())
            {
                Empleado emp = new Empleado();

                emp.Id = int.Parse(hidId.Text);
                emp.Nombre = txtNom.Text;
                emp.Apellido = txtApe.Text;
                emp.Cuil = int.Parse(txtCuil.Text);
                emp.Legajo = int.Parse(txtLeg.Text);

                repo_Emp.ModificaEmpleado(emp);

                hidId.Text = string.Empty;
                txtNom.Text = string.Empty;
                txtApe.Text = string.Empty;
                txtCuil.Text = string.Empty;
                txtLeg.Text = string.Empty;
                btnCancelar_Empleado.Visible = false;
                btnGuardar_Empleado.Visible = false;
                btnAgregar_Empleado.Visible = true;

                Bound();
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {

            btnCancelar_Empleado.Visible = false;
            btnGuardar_Empleado.Visible = false;
            btnAgregar_Empleado.Visible = true;

            hidId.Text = string.Empty;
            txtNom.Text = string.Empty;
            txtApe.Text = string.Empty;
            txtCuil.Text = string.Empty;
            txtLeg.Text = string.Empty;
        }
        #endregion


        #region Concepto

        private bool Verificacion_Concepto()
        {
            if (txtDescripcion.Text != string.Empty &&
                txtPorcentaje.Text != string.Empty)
            {
                return true;
            }

            ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), "alert('Por Favor verifique que todos los campos tengan informacion')", true);
            return false;
        }

        protected void grdConceptos_RowDeleting(object sender, GridViewDeleteEventArgs e) { }

        public void Bound_grdConcepto()
        {
            grdConceptos.DataSource = repo_Con.ListarConceptos();
            grdConceptos.DataBind();
        }

        protected void btnGuardar_Concepto_Click(object sender, EventArgs e)
        {
            if (Verificacion_Concepto())
            {
                Concepto con = new Concepto();

                con.Id = int.Parse(hidIdConcepto.Text);
                con.Descripcion = txtDescripcion.Text;
                con.PorcentajeAplicado = short.Parse(txtPorcentaje.Text);

                repo_Con.ModificaConcepto(con);

                txtDescripcion.Text = string.Empty;
                txtPorcentaje.Text = string.Empty;
                btnCancelar_Concepto.Visible = false;
                btnGuardar_Concepto.Visible = false;
                btnAgregar_Concepto.Visible = true;

                Bound_grdConcepto();
            }
        }

        protected void btnCancelar_Concepto_Click(object sender, EventArgs e)
        {
            txtDescripcion.Text = string.Empty;
            txtPorcentaje.Text = string.Empty;
            btnCancelar_Concepto.Visible = false;
            btnGuardar_Concepto.Visible = false;
            btnAgregar_Concepto.Visible = true;
        }

        protected void btnAgregar_Concepto_Click(object sender, EventArgs e)
        {
            if (Verificacion_Concepto())
            {
                Concepto con = new Concepto();

                con.Descripcion = txtDescripcion.Text;
                con.PorcentajeAplicado = short.Parse(txtPorcentaje.Text);

                repo_Con.InsertarConcepto(con);

                Bound_grdConcepto();
            }
        }

        protected void grdConceptos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int Id = int.Parse(grdConceptos.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text);
            Concepto con = repo_Con.GetConcepto(Id);
            switch (e.CommandName)
            {
                case "Select":

                    btnCancelar_Concepto.Visible = true;
                    btnGuardar_Concepto.Visible = true;
                    btnAgregar_Concepto.Visible = false;

                    hidIdConcepto.Text = con.Id.ToString();
                    txtDescripcion.Text = con.Descripcion;
                    txtPorcentaje.Text = con.PorcentajeAplicado.ToString();

                    break;
                case "Delete":

                    repo_Con.EliminaConcepto(Id);
                    break;
            }
            Bound_grdConcepto();
        }

        #endregion
        protected void MenuTab_MenuItemClick(object sender, MenuEventArgs e)
        {
            int id = Int32.Parse(e.Item.Value);
            MultiView1.ActiveViewIndex = id;

            btnGuardar_Concepto.Visible = false;
            btnCancelar_Concepto.Visible = false;
            btnAgregar_Concepto.Visible = true;
            if (id == 1)
            {
                Bound_grdConcepto();
            }
        }
    }
}