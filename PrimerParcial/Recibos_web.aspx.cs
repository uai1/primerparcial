﻿using BusinessRules;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrimerParcial
{
    public partial class Recibos_web : System.Web.UI.Page
    {
        private RepositorioConcepto repo_Con = new RepositorioConcepto();
        private RepositorioRecibo repo_Rec = new RepositorioRecibo();

        protected void Page_Load(object sender, EventArgs e)
        {
            Bound_grdConcepto();
            Bound_grdRecibos();
        }

        public void Bound_grdConcepto()
        {
            grdConceptos.DataSource = repo_Con.ListarConceptos();
            grdConceptos.DataBind();
        }

        private bool Verificacion()
        {
            if (txtDate.Text != string.Empty &&
                txtSueldoBru.Text != string.Empty)
            {
                return true;
            }

            ClientScript.RegisterClientScriptBlock(typeof(Page), Guid.NewGuid().ToString(), "alert('Por Favor verifique que todos los campos tengan informacion')", true);
            return false;
        }

        public void Bound_grdRecibos()
        {
            grdRecibos.DataSource = repo_Rec.ConsultaRecibo(int.Parse(Request.QueryString["param"]));
            grdRecibos.DataBind();
        }

        protected void btnGenerar_Recibo_Click(object sender, EventArgs e)
        {
            if (Verificacion())
            {
                Recibo rec = new Recibo();
                rec.Conceptos = new List<Concepto>();

                foreach (GridViewRow gvr in grdConceptos.Rows)
                {
                    if (((System.Web.UI.HtmlControls.HtmlInputCheckBox)gvr.Cells[0].Controls[1]).Checked == true)
                    {
                        Concepto con = new Concepto();

                        con.Id = int.Parse(gvr.Cells[1].Text);
                        con.Descripcion = gvr.Cells[2].Text;
                        con.PorcentajeAplicado = int.Parse(gvr.Cells[3].Text);

                        rec.Conceptos.Add(con);
                    }
                }
                rec.Empleado_Id = int.Parse(Request.QueryString["param"]);
                rec.SueldoBruto = int.Parse(txtSueldoBru.Text);
                rec.Fecha = Convert.ToDateTime(txtDate.Text);

                repo_Rec.InsertarRecibo(rec);
            }
        }

        protected void btnAtras_Click(object sender, EventArgs e)
        {
            Response.Redirect("Main.aspx");
        }
    }
}