﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="PrimerParcial.Main" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apliacion</title>
    <style type="text/css">
        .content{
            margin-top: 10px;
        }
        .tabs{
            position: relative;
            top: 1px;
            left: 10px;
        }
        .tab{
            border: solid 1px black;
            background-color: lightgray;
            padding: 5px 10px;
        }
        .selectedTab{
            background-color: silver;
            border-bottom: solid 1px black;
        }
        .tabContents {
            border: solid 1px black;
            padding: 10px;
            background-color: lightgray;
        }
        #txtLeg {
            height: 20px;
            margin-left: 6px;
        }
        #txtCuil {
            margin-left: 16px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
            <asp:Menu ID="MenuTab" runat="server"
                Font-Size="Large" StaticMenuItemStyle-CssClass ="tab" StaticSelectedStyle-CssClass="selectedTab" 
                StaticMenuItemStyle-HorizontalPadding="50px" StaticSelectedStyle-BackColor="White"               
               Orientation="Horizontal" CssClass="tabs" OnMenuItemClick="MenuTab_MenuItemClick">
               <Items>
                   <asp:MenuItem text="Empleados" Value="0" selected="true"/>
                   <asp:MenuItem text="Conceptos"  Value="1"/>
               </Items>
           </asp:Menu>
           <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
               <asp:View ID="View1" runat="server">
                   <div class="content">
                               <asp:TextBox ID="hidId" runat="server" Visible="false"/>
                        <div class="field">
                            <label id="lblNombre">Nombre: </label>
                            <asp:TextBox ID="txtNom" runat="server"/>
                        </div>

                        <div class="field">
                            <label id="lblApellido">Apellido: </label>
                            <asp:TextBox ID="txtApe" runat="server"/>
                        </div>

                        <div class="field">
                            <label id="lblLegajo">Legajo: </label>
                            <asp:TextBox ID="txtLeg" runat="server"/>

                        </div>

                        <div class="field">
                            <label id="lblCuil">CUIL: </label>
                            <asp:TextBox ID="txtCuil" runat="server"/>
                        </div>
                        <br />

                        <asp:Button id="btnGuardar_Empleado" Text="Guardar" runat="server" OnClick="btnGuardar_Click"  />
                        <asp:Button id="btnCancelar_Empleado" Text="Cancelar" runat="server" OnClick="btnCancelar_Click"/>
                        <asp:Button id="btnAgregar_Empleado" Text="Agregar" runat="server" OnClick="btnAgregar_Click" />
                    </div>

                    <asp:GridView id="grdEmpleados" runat="server" AutoGenerateColumns="False" 
                    HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="grdEmpleados_RowCommand" 
                        OnRowDeleting="grdEmpleados_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderStyle-Width="15" >
                            <HeaderStyle Width="15px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" HeaderStyle-Width="70" >
                            <HeaderStyle Width="70px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:BoundField DataField="Apellido" HeaderText="Apellido" HeaderStyle-Width="70" >
                            <HeaderStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Legajo" HeaderText="Legajo" HeaderStyle-Width="70" >
                            <HeaderStyle Width="70px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Cuil" HeaderText="Cuil" HeaderStyle-Width="70" >
                            <HeaderStyle Width="70px" />
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select"/>
                            <asp:ButtonField ButtonType="Button" CommandName="Delete" Text="Borrar"/>
                            <asp:ButtonField ButtonType="Button" CommandName="Recibos" Text="Recibos" />
                        </Columns>

                        <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
                    </asp:GridView>

            </asp:View>
            <asp:View ID="View2" runat="server">
                 <div class="content">
                     
                        <asp:TextBox ID="hidIdConcepto" runat="server" Visible="false"/>
                        <div class="field">
                            <label id="lblDescripcion">Descripcion: </label>
                            <asp:TextBox ID="txtDescripcion" runat="server"/>
                        </div>

                        <div class="field">
                            <label id="lblPorcentajeAplicado">Porcentaje Aplicado: </label>
                            <asp:TextBox ID="txtPorcentaje" runat="server"/>
                        </div>

                        <asp:Button id="btnGuardar_Concepto" Text="Guardar" runat="server" OnClick="btnGuardar_Concepto_Click"  />
                        <asp:Button id="btnCancelar_Concepto" Text="Cancelar" runat="server" OnClick="btnCancelar_Concepto_Click"/>
                        <asp:Button id="btnAgregar_Concepto" Text="Agregar" runat="server" OnClick="btnAgregar_Concepto_Click" />
                    </div>

                    <asp:GridView id="grdConceptos" runat="server" AutoGenerateColumns="False" 
                    HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="grdConceptos_RowCommand" OnRowDeleting="grdConceptos_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderStyle-Width="15" >
                            </asp:BoundField>
                            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" HeaderStyle-Width="200" >
                            </asp:BoundField>
                            <asp:BoundField DataField="PorcentajeAplicado" HeaderText="Porcentaje" HeaderStyle-Width="70" >
                            </asp:BoundField>
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select"/>
                            <asp:ButtonField ButtonType="Button" CommandName="Delete" Text="Borrar"/>
                        </Columns>

                        <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
                    </asp:GridView>
            </asp:View>
        </asp:MultiView>
    </form>
</body>
</html>
